package ru.tsk.ilina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractOwnerEntity extends AbstractEntity {

    private String userId;

}
