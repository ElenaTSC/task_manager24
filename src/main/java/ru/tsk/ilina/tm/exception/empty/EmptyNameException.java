package ru.tsk.ilina.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty");
    }

    public EmptyNameException(@NotNull final String message) {
        super("Error! " + message + " name is empty");
    }

}
