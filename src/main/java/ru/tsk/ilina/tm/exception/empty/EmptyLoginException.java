package ru.tsk.ilina.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty");
    }

    public EmptyLoginException(@NotNull final String message) {
        super("Error! " + message + " login is empty");
    }

}
