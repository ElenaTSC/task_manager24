package ru.tsk.ilina.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! UserId is empty");
    }

    public EmptyUserIdException(@NotNull final String message) {
        super("Error! " + message + " userid is empty");
    }

}
