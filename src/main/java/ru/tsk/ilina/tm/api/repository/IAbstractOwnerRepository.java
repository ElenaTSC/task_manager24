package ru.tsk.ilina.tm.api.repository;

import ru.tsk.ilina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IAbstractOwnerRepository<E extends AbstractOwnerEntity> extends IAbstractRepository<E> {

    E removeByID(final String userId, final String id);

    void remove(String userId, final E entity);

    void add(final String userId, final E entity);

    Integer getSize(final String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    List<E> findAll(String userId);

    boolean existsById(String userId, String id);

    void clear(String userId);

    E findByID(final String userId, final String id);

    E findByIndex(final String userId, final Integer index);

    E removeByIndex(final String userId, final Integer index);
}
