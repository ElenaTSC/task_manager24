package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.command.AbstractCommand;
import ru.tsk.ilina.tm.model.Command;

import java.util.Collection;

public interface ICommandService {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getCommand();

    Collection<AbstractCommand> getArguments();

    Collection<String> getListCommandName();

    Collection<String> getListCommandArg();

    void add(AbstractCommand command);

}
