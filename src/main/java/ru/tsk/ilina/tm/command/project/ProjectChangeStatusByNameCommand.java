package ru.tsk.ilina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractProjectCommand;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project_change_status_by_name";
    }

    @Override
    public String description() {
        return "Change status by project name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER NAME]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusValue);
        @NotNull final Project project = serviceLocator.getProjectService().changeStatusByName(userId, name, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
