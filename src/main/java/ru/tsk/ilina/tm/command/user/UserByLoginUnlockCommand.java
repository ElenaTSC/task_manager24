package ru.tsk.ilina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractAuthUserCommand;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class UserByLoginUnlockCommand extends AbstractAuthUserCommand {

    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    public String description() {
        return "Unlock user by login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("[ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
