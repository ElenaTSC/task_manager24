package ru.tsk.ilina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.command.AbstractAuthUserCommand;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class AuthLoginCommand extends AbstractAuthUserCommand {

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login in system";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        System.out.println("[OK]");
    }

}
