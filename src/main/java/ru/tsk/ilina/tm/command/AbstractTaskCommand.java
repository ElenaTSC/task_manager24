package ru.tsk.ilina.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.exception.empty.EmptyNameException;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
    }

    protected Task create(@NotNull final String name, @NotNull final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}
