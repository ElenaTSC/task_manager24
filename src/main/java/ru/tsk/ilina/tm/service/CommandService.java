package ru.tsk.ilina.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.api.repository.ICommandRepository;
import ru.tsk.ilina.tm.api.service.ICommandService;
import ru.tsk.ilina.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandService implements ICommandService {

    public final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        if (name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        if (arg.isEmpty()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommand() {
        return commandRepository.getCommand();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandName();
    }

    @Override
    public Collection<String> getListCommandArg() {
        return commandRepository.getCommandArg();
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        commandRepository.add(command);
    }
}
